#include"../../include/polyorbite_rover_hardware_library/hardware/JetsonI2CManager.h"

using namespace std;


JetsonI2CManager::JetsonI2CManager(char* bus_path, char* slave_addr){
    bus_path_ = bus_path;
    slave_addr_ = slave_addr;
    file_ID = open(bus_path,O_RDWR);
    if(file_ID < 0){
        cout << "probleme d'ouverture du bus I2C" << endl;
        exit(1);
    }
    if(ioctl(file_ID,I2C_SLAVE,slave_addr_)<0){
        cout << "probleme d'addressage du slave I2C" << endl;
        exit(1);
    }
}
JetsonI2CManager::~JetsonI2CManager(){
    if(file_ID)
        close(file_ID);
}
char JetsonI2CManager::readByte(){
    char* buffer;
    read(file_ID,buffer,sizeof(char));
    return *buffer;
}
void JetsonI2CManager::writeByte(char byte){
    char* buffer = & byte;
    write(file_ID,buffer,sizeof(char));
}
char* JetsonI2CManager::readByteData(int length){
    char buffer[length];
    char* Data = &buffer;
    read(file_ID,buffer,sizeof(char)*length);
    return Data;
}
void JetsonI2CManager::writeByteData(char* Data, int length){
    write(file_ID,Data,sizeof(char)*length);
}
short* JetsonI2CManager::readWordData(int length){
    short buffer[length];
    short* Data = &buffer;
    read(file_ID,buffer,sizeof(short)*length);
    return Data;
}
void JetsonI2CManager::writeWordData(short* Data, int length){
    short* buffer = &word;
    write(file_ID, Data,sizeof(word)*length);
}
void JetsonI2CManager::readI2CBlockData(char* buffer, int length){
    read(file_ID,buffer,length);
}
void JetsonI2CManager::writeI2CBlockData(const char* ptr_block, int length){
    write(file_ID,ptr_block,sizeof(char)*length);
}
int main(){//pour test
    JetsonI2CManager("/dev/i2c-1");
}

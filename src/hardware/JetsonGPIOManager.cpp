#include "polyorbite_rover_hardware_library/hardware/JetsonGPIOManager.h"

void JetsonGPIOManager::setPin(const uint32_t& index, const GPIO::Value& value)
{
    if(!isGPIOExported(index))
        exportGPIO(index);
    
    setGPIODirection(index, GPIO::Direction::OUT);
    setGPIOValue(index, value);
}

GPIO::Value JetsonGPIOManager::getPin(const uint32_t& index)
{
    if(!isGPIOExported(index))
        exportGPIO(index);

    setGPIODirection(index, GPIO::Direction::IN);
    return getGPIOValue(index);
}

void JetsonGPIOManager::exportGPIO(const uint32_t& index)
{
    std::ofstream exportStream;
    exportStream.open("/sys/class/gpio/export");
    exportStream << index;
    exportStream.close();

    if(_exportedGPIO.find(index) == _exportedGPIO.end())
        _exportedGPIO.insert(index);

    std::this_thread::sleep_for(std::chrono::milliseconds(1000));
}

void JetsonGPIOManager::unexportGPIO(const uint32_t& index)
{
    std::ofstream unexportStream;
    unexportStream.open("/sys/class/gpio/unexport");
    unexportStream << index;
    unexportStream.close();

    _exportedGPIO.erase(_exportedGPIO.find(index));
}

bool JetsonGPIOManager::isGPIOExported(const uint32_t& index)
{
    return boost::filesystem::exists("/sys/class/gpio/gpio" + std::to_string(index));
}

void JetsonGPIOManager::setGPIODirection(const uint32_t& index, const GPIO::Direction& direction)
{
    std::ofstream directionStream;
    directionStream.open("/sys/class/gpio/gpio" + std::to_string(index) + "/direction");
    switch(direction)
    {
        case GPIO::Direction::IN:
            directionStream << "in";
            break;
        case GPIO::Direction::OUT:
            directionStream << "out";
            break;
    }
    directionStream.close();
}

void JetsonGPIOManager::setGPIOValue(const uint32_t& index, const GPIO::Value& value)
{
    std::ofstream valueStream;
    valueStream.open("/sys/class/gpio/gpio" + std::to_string(index) + "/value");
    switch(value)
    {
        case GPIO::Value::HIGH:
            valueStream << "1";
            break;
        case GPIO::Value::LOW:
            valueStream << "0";
            break;
    }
    valueStream.close();
}

GPIO::Value JetsonGPIOManager::getGPIOValue(const uint32_t& index)
{
    std::ifstream valueStream;
    valueStream.open("/sys/class/gpio/gpio" + std::to_string(index) + "/value");
    
    std::string value;
    valueStream >> value;

    valueStream.close();

    if(value == "1") return GPIO::Value::HIGH;
    else return GPIO::Value::LOW;
}

JetsonGPIOManager::~JetsonGPIOManager()
{
    for(auto gpioIndex : _exportedGPIO)
        unexportGPIO(gpioIndex);
}
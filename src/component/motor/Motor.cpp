#include "polyorbite_rover_hardware_library/component/Motor.h"

bool Motor::getDirection(const float& velocity) {
    return velocity >= 0; // true = DIR_FORWARD and false = DIR_BACKWARD
}

void Motor::setVelocity(const float& velocity) {
    // TODO: interface with the hardware to set velocity and direction
    //        using GPIOManager and I2CManager.
    bool direction = getDirection(velocity); 

    float absVelocity = abs(velocity);

    // Set direction
    if (direction == DIR_FORWARD && _directionPinForwardValue) {
        _gpioManager.setPin(_directionPinIndex, GPIO::Value::HIGH);
    } else if (direction == DIR_FORWARD && !_directionPinForwardValue) {
        _gpioManager.setPin(_directionPinIndex, GPIO::Value::LOW);
    } else if (direction == DIR_BACKWARD && _directionPinForwardValue) {
        _gpioManager.setPin(_directionPinIndex, GPIO::Value::LOW);
    } else if (direction == DIR_BACKWARD && !_directionPinForwardValue) {
        _gpioManager.setPin(_directionPinIndex, GPIO::Value::HIGH);
    }
    
    // Set PWM
    _pwmManager.setPin(_pwmPinIndex, absVelocity);
}

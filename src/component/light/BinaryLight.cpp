#include "polyorbite_rover_hardware_library/component/BinaryLight.h"

void BinaryLight::turnOn()
{
    _gpioManager.setPin(_pinIndex, GPIO::Value::HIGH);
    _currentState = true;
}

void BinaryLight::turnOff()
{
    _gpioManager.setPin(_pinIndex, GPIO::Value::LOW);
    _currentState = false; 
}

void BinaryLight::toggle()
{
    _currentState = !_currentState;
    _gpioManager.setPin(_pinIndex, (GPIO::Value)_currentState);
}

bool BinaryLight::getValue()
{
    return _currentState;
}
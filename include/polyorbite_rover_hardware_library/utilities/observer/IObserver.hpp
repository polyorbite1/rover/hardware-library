#pragma once

class IObserver
{
public:
    virtual void notify(char* message) = 0;
};
#pragma once

#include "polyorbite_rover_hardware_library/utilities/observer/IObserver.hpp"

template<class T>
class Observer : public IObserver
{
public:
    void notify(char* message) override
    {
        onNotified(*((T*)message));
    }

protected:
    virtual void onNotified(const T& message) = 0;
};
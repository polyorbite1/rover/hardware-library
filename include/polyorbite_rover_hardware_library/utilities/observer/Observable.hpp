#pragma once

#include <list>
#include "polyorbite_rover_hardware_library/utilities/observer/IObserver.hpp"

template<class T>
class Observable
{
public:
    void observe(IObserver* observer)
    {
        _observers.push_back(observer);
    }

    void lookAway(IObserver* observer)
    {
        _observers.remove(observer);
    }

protected:
    void notifyObservers(const T& message)
    {
        for(auto observer : _observers)
        {
            observer->notify((char*)(&message));
        }
    }

private:
    std::list<IObserver *> _observers;
};
#pragma once

#define DIR_FORWARD true
#define DIR_BACKWARD false

class IMotor
{
public:
    virtual void setVelocity(const float& velocity) = 0;
};
#pragma once

#include "polyorbite_rover_hardware_library/utilities/observer/Observable.hpp"

class IMotorEncoder : public Observable<float> {};
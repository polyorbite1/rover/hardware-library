#pragma once

#include "polyorbite_rover_hardware_library/utilities/observer/Observable.hpp"
#include "polyorbite_rover_hardware_library/component-data/Power.h"

class IPowerMonitor : public Observable<Power> 
{
public:
    PowerMonitor(
        int32& i2cBusIndex;
        int32& PollingFrequency;
    ) : _i2cBusIndex(i2cBusIndex), _PollingFrequency(PollingFrequency)
    {}

    void ReadPower()
    {
        return Power.power, Power.current, Power.tension; 
    }

    std::thread PowerThread(ReadPower)

private:
    int32 _i2cBusIndex;
    int32 _PollingFrequency;
}
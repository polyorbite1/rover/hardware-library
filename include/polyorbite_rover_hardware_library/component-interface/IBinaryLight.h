#pragma once

class IBinaryLight
{
public:
    virtual void turnOn() = 0;
    virtual void turnOff() = 0;
    virtual void toggle() = 0;
    virtual bool getValue() = 0;
};
#pragma once

#include <stdint.h>
#include <stdlib.h>
#include "polyorbite_rover_hardware_library/component-interface/IMotor.h"
#include "polyorbite_rover_hardware_library/hardware/JetsonGPIOManager.h"
#include "polyorbite_rover_hardware_library/hardware/PWMManager.h"

class Motor : IMotor
{
public:
    Motor(
        const uint32_t& directionPinIndex,
        const bool& directionPinForwardValue,
        const uint32_t& pwmPinIndex
    ) :   
    _directionPinIndex(directionPinIndex),
    _directionPinForwardValue(directionPinForwardValue),  
    _pwmPinIndex(pwmPinIndex)
    {

    }

    void setVelocity(const float& velocity) override;

private: 
    uint32_t _directionPinIndex;
    bool _directionPinForwardValue;
    uint32_t _pwmPinIndex;
    JetsonGPIOManager _gpioManager;
    PWMManager _pwmManager;

    bool getDirection(const float& velocity);
};
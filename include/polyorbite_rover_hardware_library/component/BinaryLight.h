#pragma once

#include <stdint.h>
#include "polyorbite_rover_hardware_library/component-interface/IBinaryLight.h"
#include "polyorbite_rover_hardware_library/hardware/JetsonGPIOManager.h"

class BinaryLight : IBinaryLight
{
public:
    BinaryLight(
        const bool& initialValue,
        const uint32_t& pinIndex
    ) : _initialValue(initialValue), _pinIndex(pinIndex), _currentState(initialValue)
    {}

    void turnOn() override;
    void turnOff() override;
    void toggle() override;
    bool getValue() override;

private:
    bool _initialValue;
    uint32_t _pinIndex;

    bool _currentState;
    JetsonGPIOManager _gpioManager;
};
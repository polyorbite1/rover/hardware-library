#pragma once

struct Power
{
    float power;
    float current;
    float tension;
};
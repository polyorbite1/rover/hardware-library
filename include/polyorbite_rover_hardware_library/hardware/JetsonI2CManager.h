#include"I2CManager.h"

#define MAX_SLAVE 1;

class JetsonI2CManager : public I2CManager
{
    public:
        //constructeur de la classe initialise la connexion et prend
        //le repertoire du bus ainsi que le nombre de slave et leur adresse
        JetsonI2CManager(char* bus_path, char* slave_addr);
        //destructeur: ferme la communication du bus
        ~JetsonI2CManager();
        //lis un char sur le bus I2C
        char readByteData() override;
        //ecris un char sur le bus I2C
        void writeByteData(char byte, int length) override;
        //lis un short sur le bus I2C
        short readWordData(int length) override;
        //ecris un short sur le bus I2C
        void writeWordData(short word, int length) override;
        //lis un tableau de byte sur le bus I2C
        //prend ladresse du tableau de char a remplir et la taille en bite du message
        void readI2CBlockData(char* buffer,int length) override;
        //ecris sur le bus le buffer a envoyer
        void writeI2CBlockData(const char* buffer) override;
    private:
        //identificateur du fichier d'ecriture/lecture du bus I2C
        int file_ID=-1;
        char* bus_path_;
        char* slave_addr_;
};
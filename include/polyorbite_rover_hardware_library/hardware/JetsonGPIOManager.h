#pragma once

#include <chrono>
#include <thread>

#include <stdint.h>
#include <fstream>
#include <string>
#include <set>
#include "boost/filesystem.hpp"

#include "polyorbite_rover_hardware_library/hardware/IGPIOManager.h"

class JetsonGPIOManager : public IGPIOManager
{
public:
    void setPin(const uint32_t& index, const GPIO::Value& value) override;
    GPIO::Value getPin(const uint32_t& index) override;

    ~JetsonGPIOManager();

private:
    std::set<uint32_t> _exportedGPIO;

private:
    void exportGPIO(const uint32_t& index);
    void unexportGPIO(const uint32_t& index);
    bool isGPIOExported(const uint32_t& index);

    void setGPIODirection(const uint32_t& index, const GPIO::Direction& direction);
    void setGPIOValue(const uint32_t& index, const GPIO::Value& value);
    GPIO::Value getGPIOValue(const uint32_t& index);
};
#include<stdint.h>
#include<iostream>
#include<linux/i2c-dev.h>
#include<sys/ioctl.h>
#include<unistd.h>
#include<fcntl.h>

class I2CManager
{
    public:
    virtual char readByteData(int length)=0;
    virtual void writeByteData(char byte, int length)=0;
    virtual short readWordData(int length)=0;
    virtual void writeWordData(short word, int length)=0;
    virtual void readI2CBlockData(char* buffer, int length)=0;
    virtual void writeI2CBlockData(const char* ptr_block, int length)=0;
    };
#pragma once

#include <stdint.h>

class IPWMManager {
public:
    virtual void setPin(const uint32_t& index, const float& value) = 0;
    virtual float getPin(const uint32_t& index) = 0;
};
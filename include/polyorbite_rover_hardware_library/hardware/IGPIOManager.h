#pragma once

#include <stdint.h>

namespace GPIO {
    enum Direction {
        IN = 0,
        OUT = 1
    };

    enum Value {
        LOW = 0,
        HIGH = 1
    };
};

class IGPIOManager
{
public:
    virtual void setPin(const uint32_t& index, const GPIO::Value& value) = 0;
    virtual GPIO::Value getPin(const uint32_t& index) = 0;
};
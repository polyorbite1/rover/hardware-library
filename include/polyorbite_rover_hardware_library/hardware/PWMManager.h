#pragma once

#include "polyorbite_rover_hardware_library/hardware/IPWMManager.h"

class PWMManager : public IPWMManager 
{
public:
    void setPin(const uint32_t& index, const float& value) override;
    float getPin(const uint32_t& index) override;

private:
};